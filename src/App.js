///import React from 'react'



///import { Resturant } from './component/Basics/Resturant'; ///>>> yedi export default garena bhane chai yo syntax use garnu parxa hai ta



import { useState } from 'react/cjs/react.development';
import Resturant from './component/Basics/Resturant';
import UseState from './component/Hooks/useState';
import UseEffect from './component/Hooks/useEffect';
import UseReducer from './component/Hooks/useReducer';


const App = () => {   /// component define garda kheri >>> export const App=()=>{}  <<< yesari define gare ta pani tala export default App chai garnae parxa hai ta
  return (
    <>
      <UseReducer />
    </>
  )
}



export default App




/// example :
///export const App = () => {
///  return (
///    <div>
///      <Resturant />  /// calling a component or functional component present in Resturant.js
///      <Joseph />
///      <Joseph />
///      <Joseph />
///      <Joseph />
///      <Joseph />
///      welcome to joseph 369 {3 + 3 + 3}
///      <Joseph />
///    </div>
/// yaha <jospeh/> rakhyo bhane chai error auxa kinaki JSX haru lai wrap garnu parxa bahiri tag le.
///  )
///}
/// mathi ko code ma eutai functional component Joseph lai multiple thau ma use gariyeko xa . 



/// new component /// to demonstrate nested component
///const Joseph = () => { return <p>hello hello hello</p> }



/// without using fat arrow function we have created a function and exported it
/// a function is a functional component
/// euta functional component le jahile pani JSX element lai return gareko hunu parxa hai
/// function App() {
///   return <h1>jspeh</h1>
/// }



/// yedi hami le old react coding pattern follow garirako xum bhane chai react version >=17 bhaye pani import React chai garnai parxa hai ta
/// example of this :
/// dui ota function or say functional component ko name same huna paudaena hai react app ma
/// react version >=17 bhaye pani yedi purano way ma react code lehinxa bhane react lai import garnu parxa hai ta
/// const App = () => {
///   return React.createElement("h1", {}, "joseph 369")
/// }


